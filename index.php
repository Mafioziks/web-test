<?php

error_reporting(E_ALL);

require("./vendor/autoload.php");

use RRouter\Router;

$route = new Router();

$route->get('/^[\/]?$/', App\Controllers\SiteController::class, 'index');
$route->get('/^\/about$/', App\Controllers\SiteController::class, 'about');
$route->get('/^\/projects$/', App\Controllers\SiteController::class, 'projects');

$route->get('/^\/regex\/form$/', App\Controllers\RegexController::class, 'form');
$route->post('/^\/regex\/parse$/', App\Controllers\RegexController::class, 'parse');
$route->delete('/^\/delete$/', App\Controllers\TestController::class, 'deleteRequest');

