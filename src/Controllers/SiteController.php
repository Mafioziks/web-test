<?php

namespace App\Controllers;

class SiteController {

    private $projects = [
        "Blog" => ["url" => "http://tomsteteris.id.lv"],
        "Photo" => ["url" => "http://photo.tomsteteris.id.lv"],
    ];

    public function beforeAction()
    {
        echo "<pre>";
    }

    public function afterAction()
    {
        echo "</pre>";
    }

    public function index()
    {
        echo "Welcome to test area!";
    }

    public function about()
    {
        echo "This will be mine test are where I will test different features, packages, other things I will create.";
    }

    public function projects()
    {
        echo "<h3>My Projects</h3>";
        echo "<ul>"; 
        foreach($this->projects as $name => $project) {
            printf("<li><a href=\"%s\">%s</a></li>", $project['url'], $name);
        }
        echo "</ul>"; 
    }
}
