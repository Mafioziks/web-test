<?php
/**
 * Created by PhpStorm.
 * User: toms
 * Date: 18.26.6
 * Time: 16:15
 */

namespace App\Controllers;


class RegexController
{
    public function beforeAction()
    {
        ?>
        <!DOCTYPE html>
        <html>
            <head>
                <meta charset="utf-8" />
                <title>Test regex</title>
            </head>
            <body>
        <?php
    }


    public function afterAction()
    {
        ?>
            </body>
        </html>
        <?php
    }

    public function form()
    {
        ?>
        <form method="post" action="/regex/parse">
            <div>
                <label for="regex">Regex line</label>
                <input id="regex" type="text" name="regex">
            </div>
            <div>
                <label for="text">Text</label>
                <textarea name="text" cols="100" rows="10"></textarea>
            </div>
            <button>SUBMIT</button>
        </form>
        <?php
    }

    public function parse()
    {
        if (empty($_POST['regex']) || empty($_POST['text'])) {
            die('Empty parameters set!!!');
        }

        preg_match($_POST['regex'], $_POST['text'], $match);

        echo str_replace('  ', '&nbsp;', nl2br(print_r($match, true)));
    }
}