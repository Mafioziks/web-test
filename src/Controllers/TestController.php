<?php
/**
 * Created by PhpStorm.
 * User: toms
 * Date: 18.27.6
 * Time: 13:33
 */

namespace App\Controllers;


class TestController
{
    public function deleteRequest()
    {
        echo json_encode([
            'status'  => 'success',
            'message' => 'You just deleted something!',
            'post'    => $_POST,
        ], JSON_PRETTY_PRINT);
    }
}