<?php

namespace RRouter;

class Router {

    const GET    = 'GET';
    const POST   = 'POST';
    const PUT    = 'PUT';
    const DELETE = 'DELETE';

    const ANY    = 'ANY';

    public const HTTP_METHOD_VAR_NAME = '_method';

    public function __construct() {}

    /**
     * @param string $url
     * @param string $class
     * @param string $function
     */
    public function get(string $url, string $class, string $function)
    {
        $this->way(self::GET, $url, $class, $function);
    }

    /**
     * @param string $url
     * @param string $class
     * @param string $function
     */
    public function post(string $url, string $class, string $function)
    {
        $this->way(self::POST, $url, $class, $function);
    }

    /**
     * @param string $url
     * @param string $class
     * @param string $function
     */
    public function put(string $url, string $class, string $function)
    {
        $this->way(self::PUT, $url, $class, $function);
    }

    /**
     * @param string $url
     * @param string $class
     * @param string $function
     */
    public function delete(string $url, string $class, string $function)
    {
        $this->way(self::DELETE, $url, $class, $function);
    }

    /**
     * @param string $url
     * @param string $class
     * @param string $function
     */
    public function any(string $url, string $class, string $function)
    {
        $this->way(self::ANY, $url, $class, $function);
    }

    /**
     * @param string $method
     * @param string $url
     * @param string $class
     * @param string $function
     */
    private function way($method, $url, $class, $function)
    {
        if (self::ANY !== $method) {
            if ($method !== strtoupper($_SERVER['REQUEST_METHOD'])
                && !(
                    self::POST === strtoupper($_SERVER['REQUEST_METHOD'])
                    && (!isset($_POST[self::HTTP_METHOD_VAR_NAME]) || $method !== strtoupper($_POST[self::HTTP_METHOD_VAR_NAME]))
                )
            ) {
                error_log('Not correct HTTP REQUEST METHOD: ' . $method . ' => ' . $_SERVER['REQUEST_METHOD']);
                return;
            }
        }

        if (!preg_match($url, $_SERVER['REQUEST_URI'], $match)) {
            error_log("Regex does not match url: " . $_SERVER['REQUEST_URI']);
            return;
        }

        if (!class_exists($class)) {
            http_response_code(404);
            echo "Class not found!";
            error_log("Class {$class} not found!");
            return;
        }

        if (!method_exists($class, $function)) {
            http_response_code(404);
            echo "Method not found!";
            error_log("Method {$function} not found!");
            return;
        }

        $controller = new $class;

        if (method_exists($controller, 'beforeAction')) {
            $controller->beforeAction();
        }

        $controller->{$function}();
        
        if (method_exists($controller, 'afterAction')) {
            $controller->afterAction();
        }

        error_log("> Router success");
    }
}
