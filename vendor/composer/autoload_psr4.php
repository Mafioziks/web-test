<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'RRouter\\' => array($vendorDir . '/Mafioziks/RRouter/src'),
    'App\\' => array($baseDir . '/src'),
);
